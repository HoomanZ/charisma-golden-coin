import {createSlice} from '@reduxjs/toolkit';
const initialState = {
    url:''
}

export const getURL = createSlice({
    initialState:initialState,
    name:'url',
    reducers:{
        setURL:(state,action)=>{
            state.code=action.payload;
        }
    },
});

export const {setURL} = getURL.actions;
export default getURL.reducer;