import {createSlice} from '@reduxjs/toolkit';
const initialState = {
    code:''
}

export const reqCode = createSlice({
    initialState:initialState,
    name:'coinId',
    reducers:{
        setCoinId:(state,action)=>{
            state.code=action.payload;
        }
    },
});

export const {setCoinId} = reqCode.actions;
export default reqCode.reducer