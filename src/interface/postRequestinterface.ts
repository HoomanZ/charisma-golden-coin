export default interface postReq{
    data: any;
    message: string;
    error?: string;
    lastUrl?:string;
}