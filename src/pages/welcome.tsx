import React,{useMemo, useState} from 'react'
import { CacheProvider } from '@emotion/react';
import rtlPlugin from "stylis-plugin-rtl";
import createCache from "@emotion/cache";
import { prefixer } from "stylis";
import { Box } from '@mui/system';
import { Alert, Typography, TextField, useTheme, Checkbox, Button } from '@mui/material';
import FormControlLabel from '@mui/material/FormControlLabel';
import { Search } from '@material-ui/icons';
import { useNavigate } from 'react-router-dom';
import getReq from '../interface/getRequestinterface';
import { getRequest } from '../request';
import { toast } from 'react-toastify';
import { useMediaQuery } from 'react-responsive';
import { TokenGetter } from './tokenGetter.tsx';


function Welcome() {
  const theme = useTheme();
  const style = {
    mobileContainer:{
      display:'flex',
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'center',
      width:'82%',
      height:'75%',
      padding:'0.5rem',
      backgroundColor:theme.palette.primary.main
    },
    container:{
      display:'flex',
      flexDirection:'column',
      alignItems:'center',
      justifyContent:'center',
      width:'53%',
      height:'50%',
      padding:'0.5rem',
      backgroundColor:theme.palette.primary.main
    },
  }
    const navigate = useNavigate();
    const [isCodeExist, setIsCodeExist] = useState<boolean>(false);
    const [coinCode, setCoinCode] = useState<string>('');
    const cacheRtl = useMemo(() => {
        return  createCache({
        key: "muirtl",
        stylisPlugins: [prefixer, rtlPlugin]
      })
    }, []);
    const isDesktop = useMediaQuery({
      query: "(min-width: 1224px)"
    });

    const isTablet = useMediaQuery({
      query: "((min-width:650px) and (max-width: 768px))"
    });

    const isMobile = useMediaQuery({
      query: "(max-width: 400px)"
    });
    const handleClick=async()=>{
      const {data,error}:getReq = await getRequest(`${process.env.REACT_APP_BASE_URL}/coin/${coinCode}`) as getReq;
      if(data){
        if(data.id){
          navigate('/customerInfo',{state:{...data,assigned:true}})
          }else{
            navigate(`/GetCoin/${coinCode}`);
          }
        }
      else{
        toast.error('.کد سکه نادرست است')
      }
  }
  return (
    <div>
      <CacheProvider value={cacheRtl}>
        <div className='backgroundTemplate'>
          <TokenGetter/>
            <Box sx={{display:'flex',justifyContent:'center',marginTop:'2rem'}}>
                <Alert severity="info" sx={isMobile?{marginBottom:'4rem',display:'flex',alignItems:'center',width:'85%'}:{marginBottom:'4rem',display:'flex',alignItems:'center',}}>
                    <Box sx={{display:'flex',justifyContent:"center",}}><Typography variant="h4" sx={{fontWeight:'bold'}}>سامانه خرید سکه صرافی کاریزما</Typography></Box>
                </Alert>
            </Box>
            <Box sx={{width:'100vw',height:'50%',display:'flex',justifyContent:'center',alignItems:'center'}}>
              <Box sx={isMobile||isTablet?style.mobileContainer: style.container}>
                  <Box sx={{textAlign:'center'}}>
                    <Typography variant="h5" color='white'>کاربر گرامی در صورت دسترسی به کد سکه، کد را وارد کنید در غیر اینصورت لطفاً مجدداً سکه را اسکن نمایید.</Typography>
                  </Box>
                  <Box sx={isMobile?{display:'flex',flexDirection:'column',width:'95%',height:'4rem',alignItems:'center'}:{display:'flex',width:'95%',height:'4rem',alignItems:'center'}}>

                    <FormControlLabel
                      sx={{ color: "#fff" }}
                      control={
                        <Checkbox
                          checked={isCodeExist}
                          color="default"
                          sx={{ color: "#fff" }}
                          onChange={(e)=>setIsCodeExist(e.target.checked)}
                        />
                      }
                      label="کد سکه دارید؟"
                    />
                  {isCodeExist?
                  <Box sx={{display:'flex',alignItems:'center'}}>
                    <Button variant="text" 
                    sx={{
                      color:'#fff',
                      backgroundColor:'#cd6019',
                      '&:hover': {
                        background: "#b93e18",
                     }
                  }} onClick={handleClick}>
                      جستجو
                      <Search/>
                    </Button>
                    <TextField
                      size='small'
                      label="کد سکه"
                      value={coinCode}
                      onChange={(e)=>setCoinCode(e.target.value)}
                      style={isMobile?{width:'13rem'}:isTablet?{width:'16rem'}:{width:'20rem',}}
                      sx={{
                        
                        "& .MuiInputBase-root": {
                            color: '#efefef'
                        },
                        "& .MuiFormLabel-root": {
                            color: '#efefef'
                        },
                        "& .MuiFormLabel-root.Mui-focused": {
                            color: '#efefef'
                        },
                        
                        '& .MuiOutlinedInput-root': {
                            "& .MuiAutocomplete-endAdornment":{
                                color: '#efefef'
                            },
                            '& fieldset': {
                              borderColor: '#efefef',
                            },
                            '&:hover fieldset': {
                              borderColor: '#efefef',
                            },
                            '&.Mui-focused fieldset': {
                              borderColor: '#efefef',
                            },
                        },
                      }}
                    />
                  </Box>
                  :null
                  }
                  </Box>
              </Box>
            </Box>
        </div>
      </CacheProvider>
    </div>
  )
}

export default Welcome
