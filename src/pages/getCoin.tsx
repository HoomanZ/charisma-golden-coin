import React, { useState, useEffect } from 'react';
import rtlPlugin from "stylis-plugin-rtl";
import { CacheProvider } from "@emotion/react";
import createCache from "@emotion/cache";
import { prefixer } from "stylis";
import Search from '@mui/icons-material/Search';
import { useNavigate, useParams } from 'react-router-dom';
import { getRequest } from '../request';
import getReq from '../interface/getRequestinterface';
import { toast } from 'react-toastify';
import '../css/searchRequest.scss';
import { useDispatch } from 'react-redux';
import {setCoinId} from '../redux/coinId';
import { TokenGetter } from './tokenGetter.tsx';

const style={
    formTitle:{
      fontWeight:'bold !important' 
    },
    formDescription:{
      color:'#9b9b9b'
    },
    formContainer:{
      padding: '1rem !important',
      border: '3px solid #748DA6',
      borderRadius: '10px',
      backgroundColor: '#f1f7ff59',
      height:'fit-content'
    },
    gridBox:{
      display:'grid',
      gridTemplateColumns:'40% 40%',
      columnGap:'5%',
      rowGap:'3%',
      justifyContent:'space-around',
    },
    mobileGridBox:{
      display:'grid',
      gridTemplateColumns:'90%',
      // columnGap:'5%',
      rowGap:'3%',
      justifyContent:'center',
      marginTop:'2rem'
    },
    modalStyle :{
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      p: 4,
      bgcolor: 'background.paper',
    },
    txtField:{
      width:'100%', 
    },
    headerContainer:{
      display:'flex',
      width:'100%',
      justifyContent:'space-between'
    },
    stateHandler:{
      display:'flex',
    },
    stateType:{fontWeight:'bold'},  
  }
function GetCoin() {
  const dispatch = useDispatch()
    const cacheRtl = createCache({
        key: "muirtl",
        stylisPlugins: [prefixer, rtlPlugin]
      });
    const navigate = useNavigate();
    const [requestNumber, setRequestNumber] = useState('');
    useEffect(() => {
      searchCoin();
    }, []);
    const params = useParams();
    const searchCoin=async()=>{
        const data:getReq = await getRequest(`${process.env.REACT_APP_BASE_URL}/coin/${params.coinId}`) as getReq;
        if(!data.error){
            dispatch(setCoinId(params.coinId!));
            if(data.data.id){
              navigate('/customerInfo',{state:{...data.data,assigned:true}});
            }
        }else if(data.error === '!لطفا دوباره وارد شوید'){
            navigate('/login');
            toast.error(data.error);
        }else{
          toast.error(data.error);
        }
    }
    const searchRequest=async()=>{
      if(isNaN(+requestNumber) || requestNumber.length!== 10){
        toast.error('!کد ملی نادرست است');
      }else{
        const {data,error}:getReq = await getRequest(`${process.env.REACT_APP_BASE_URL}/Customer/${requestNumber}`) as getReq;
        if(!error){
          if(data === "مشتری یافت نشد!"){
            navigate('/userSendInfo')
          }else{
            navigate('/CustomerInfo',{state:data});
          }
        }else{
            toast.error(data.error);
        }
      }
    }
    const handleBtnSelect=(e)=>{
        if(e.key === 'Enter'){
            searchRequest();
        }
    }
  return (
    <CacheProvider value={cacheRtl}>
        <div className='backgroundTemplate'>
          <TokenGetter/>
          <div style={{display:'flex',justifyContent:'center',marginTop:'5rem'}}>
              <div className="SearchCard">
                  <div className="SearchCardInner">
                      <label className='labelsearch'>کد ملی را وارد کنید</label>
                      <div className="SearchContainer">
                          <div className="Icon" onClick={searchRequest}>
                              <Search/>
                          </div>
                          <div className="InputContainer">
                              <input 
                              className='SearchInput' 
                              placeholder="کد ملی" 
                              onChange={e=>setRequestNumber(e.target.value)} 
                              onKeyDown={(e)=>handleBtnSelect(e)}/>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
        </div>
    </CacheProvider>
  )
}

export default GetCoin
