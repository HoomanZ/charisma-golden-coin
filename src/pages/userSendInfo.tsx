import React,{useReducer,useMemo, useEffect} from 'react';
import { Alert, Box, Button, TextField, Typography } from '@mui/material';
import {Check} from '@material-ui/icons';
import { CacheProvider } from '@emotion/react';
import rtlPlugin from "stylis-plugin-rtl";
import createCache from "@emotion/cache";
import { prefixer } from "stylis";
import postReq from '../interface/postRequestinterface';
import { postRequest } from '../request';
import { toast } from 'react-toastify';
import { useMediaQuery } from 'react-responsive';
import { useNavigate } from 'react-router-dom';
interface customerDetails {
  
  firstName: string,
  lastName: string,
  nationalCode: string,
  address:string,
  phoneNumber: string
}

const style={
    formTitle:{
      fontWeight:'bold !important' 
    },
    formDescription:{
      color:'#9b9b9b'
    },
    formContainer:{
      padding: '1rem !important',
      border: '3px solid #748DA6',
      borderRadius: '10px',
      backgroundColor: '#f1f7ff59',
      height:'fit-content'
    },
    gridBox:{
      display:'grid',
      gridTemplateColumns:'40% 40%',
      columnGap:'5%',
      rowGap:'25%',
      alignItems:'center',
      justifyContent:'space-around',
      width:'90vw'
    },
    mobileGridBox:{
      display:'grid',
      gridTemplateColumns:'90%',
      // columnGap:'5%',
      rowGap:'3%',
      justifyContent:'center',
      marginTop:'2rem'
    },
    modalStyle :{
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      p: 4,
      bgcolor: 'background.paper',
    },
    txtField:{
      width:'100%', 
    },
    headerContainer:{
      display:'flex',
      width:'100%',
      justifyContent:'space-between'
    },
    stateHandler:{
      display:'flex',
      alignItems:'center',
      justifyContent:'center'
    },
    stateType:{fontWeight:'bold'},  
    tabletInputs:{
      backgroundColor:'#efefef82',
      width:'30rem'
    },
    mobileInputs:{
      backgroundColor:'#efefef82',
      width:'17rem'
    },
    desktopInputs:{
      backgroundColor:'#efefef82'
    }
  }
  function UserSendInfo() {
  const initialState:customerDetails={
    firstName: '',
    lastName: '',
    nationalCode: '',
    address:'',
    phoneNumber: ''
  };
  const isDesktop = useMediaQuery({
    query: "(min-width: 1224px)"
  });

  const isTablet = useMediaQuery({
    query: "((min-width:650px) and (max-width: 768px))"
  });

  const isMobile = useMediaQuery({
    query: "(max-width: 400px)"
  });
  const [state, dispatch] = useReducer(reducer, initialState);

    function reducer(state, action) {
      switch (action.type) {
        case 'UPDATE':
          return {
            ...state,
            [action.key]: action.value,
          }
        default:
          return state
      }
    }
    const cacheRtl = useMemo(() => {
      return  createCache({
      key: "muirtl",
      stylisPlugins: [prefixer, rtlPlugin]
    })
    }, []);
    
    const navigate = useNavigate();
    const handleSubmit = async(e)=>{
      e.preventDefault();
      if(isNaN(+state.nationalCode) || state.nationalCode.length!== 10){
        toast.error('کد ملی نامعتبر است')
      }else{
        if(state.phoneNumber?.match(/^(\+\d{1,3}[- ]?)?\d{11}$/)){
          const {data,error} :postReq = await postRequest(`${process.env.REACT_APP_BASE_URL}/Customer`,state) as postReq;
          if(!error){
            toast.success("!اطلاعات با موفقیت ثبت شد");
            navigate('/customerInfo',{state:{...state,id:data}});
          }else{
            toast.error(error);
          }
        }else{
          toast.error('شماره موبایل نامعتبر است');
        }
      }
    }
  return (
    <CacheProvider value={cacheRtl}>
      <div className='backgroundTemplate'>
        <Box sx={{display:'flex',justifyContent:'center',marginTop:'2rem'}}>
          <Alert severity="info" sx={{marginBottom:'4rem',display:'flex',alignItems:'center'}}>
              <Box sx={{display:'flex',justifyContent:"center",}}><Typography variant="h4" sx={{fontWeight:'bold'}}> اطلاعات مشتری</Typography></Box>
          </Alert>
        </Box>
        <Box sx={style.stateHandler}>
          <Box sx={isTablet||isMobile?style.mobileGridBox: style.gridBox}>
              <TextField
                sx={isTablet?style.tabletInputs:isMobile?style.mobileInputs:style.desktopInputs}
                label="نام"
                name='firstName'
                value={state.firstName}
                onChange={(e)=>
                  dispatch({
                    type: 'UPDATE',
                    value: e.target.value,
                    key: 'firstName',
                  })
                }
              />
              <TextField
                sx={isTablet?style.tabletInputs:isMobile?style.mobileInputs:style.desktopInputs}
                label="نام خانوادگی"
                name='lastName'
                value={state.lastName}
                onChange={(e)=>
                  dispatch({
                    type: 'UPDATE',
                    value: e.target.value,
                    key: 'lastName',
                  })
                }
              />
              <TextField
              sx={isTablet?style.tabletInputs:isMobile?style.mobileInputs:style.desktopInputs}
                label="کد ملی"
                name='nationalCode'
                value={state.nationalCode}
                onChange={(e)=>
                  dispatch({
                    type: 'UPDATE',
                    value: e.target.value,
                    key: 'nationalCode',
                  })
                }
              />
              <TextField
                sx={isTablet?style.tabletInputs:isMobile?style.mobileInputs:style.desktopInputs}                
                label="شماره تماس"
                name='phoneNumber'
                value={state.phoneNumber}
                onChange={(e)=>
                  dispatch({
                    type: 'UPDATE',
                    value: e.target.value,
                    key: 'phoneNumber',
                  })
                }
              />
              <TextField
                sx={isTablet?style.tabletInputs:isMobile?style.mobileInputs:style.desktopInputs}                
                label="آدرس"
                name='address'
                multiline
                rows={3}
                value={state.address}
                onChange={(e)=>
                  dispatch({
                  type: 'UPDATE',
                  value: e.target.value,
                  key: 'address',
                })
              }
              />
              
              <Button 
                style={{width:'fit-content',height:'fit-content'}} 
                variant="contained" 
                color="primary" 
                onClick={handleSubmit}
                disabled={
                  !state.address||
                  !state.firstName||
                  !state.lastName||
                  !state.nationalCode||
                  !state.phoneNumber
                }>
                ثبت <Check/>
              </Button>
          </Box>
        </Box>
      </div>
    </CacheProvider>
  )
}

export default UserSendInfo
