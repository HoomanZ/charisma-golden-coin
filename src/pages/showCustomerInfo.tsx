import React, { useMemo,useEffect, useState } from 'react'
import { CacheProvider } from '@emotion/react';
import rtlPlugin from "stylis-plugin-rtl";
import createCache from "@emotion/cache";
import { prefixer } from "stylis";
import { useLocation } from 'react-router-dom';
import { Alert, Box, Button, FormControl, InputLabel, MenuItem, Modal, Select, Stack, Typography, useTheme } from '@mui/material';
import { getRequest } from '../request';
import { useSelector } from 'react-redux';
import getReq from '../interface/getRequestinterface';
import { useMediaQuery } from "react-responsive"; 
import { toast } from 'react-toastify';


function ShowCustomerInfo() {
  const theme = useTheme();
  const style={
    formTitle:{
      fontWeight:'bold !important' 
    },
    formDescription:{
      color:'#9b9b9b'
    },
    formContainer:{
      padding: '1rem !important',
      border: '3px solid #748DA6',
      borderRadius: '10px',
      backgroundColor: '#f1f7ff59',
      height:'fit-content'
    },
    gridBox:{
      display:'grid',
      gridTemplateColumns:'40% 40%',
      columnGap:'5%',
      rowGap:'6%',
      alignItems:'center',
      justifyContent:'space-around',
      width:'60vw',
      height:'38vh',
      backgroundColor:theme.palette.primary.main
    },
    mobileGridBox:{
      display:'grid',
      gridTemplateColumns:'90%',
      // columnGap:'5%',
      rowGap:'3%',
      justifyContent:'center',
      marginTop:'2rem',
      height:'100%',
      width:'80%',
      backgroundColor:theme.palette.primary.main
    },
    tabletGridBox:{
      display:'grid',
      gridTemplateColumns:'40% 40%',
      rowGap:'3%',
      columnGap:'5%',
      justifyContent:'center',
      marginTop:'2rem',
      height:'100%',
      width:'80%',
      backgroundColor:theme.palette.primary.main
    },
    modalStyle :{
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      p: 4,
      bgcolor: 'background.paper',
    },
    field:{
      textAlign:'center',
      color:'#fff'
    },
    txtField:{
      display:'flex',
      justifyContent:'center',
      alignItems:'center',
      height:'60vh' 
    },
    headerContainer:{
      display:'flex',
      width:'100%',
      height:'20vh',
      justifyContent:'center',
      alignItems:'center'
    },
    stateHandler:{
      fontWeight:'bold'
    },
    addressHandler:{
      fontWeight:'bold',
      cursor:'pointer'
    },
    stateType:{
      fontWeight:'bold'
    },
    modalMobileStyle:{
      position: 'absolute',
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      p: 4,
      backgroundColor:'#fff',
      display:'flex',
      flexDirection:'column',
      borderRadius:'3px',
      width:'15rem'
    },
  }
    const location = useLocation();
    const [openModal, setOpenModal] = useState<boolean>(false);
    const requestCode = useSelector((state:any) => state.requestCode.code);
    const [isAssigned, setIsAssigned] = useState<boolean>(false);
    const [coinType, setcoinType] = useState<string>(0);
    const cacheRtl = useMemo(() => {
        return  createCache({
        key: "muirtl",
        stylisPlugins: [prefixer, rtlPlugin]
      })
    }, []);
    const isDesktop = useMediaQuery({
      query: "(min-width: 1224px)"
    });

    const isTablet = useMediaQuery({
      query: "((min-width:650px) and (max-width: 768px))"
    });

    const isMobile = useMediaQuery({
      query: "(max-width: 400px)"
    });
    
    const handleSubmit=async()=>{
      if(requestCode){
        const {data,error}:getReq = await getRequest(`${process.env.REACT_APP_BASE_URL}/Coin/Sale/${requestCode}/${location.state.id}/${coinType}`) as getReq;
        if(!error){
          toast.success('!سکه با موفقیت ثبت شد');
          setIsAssigned(true);
        }
      }else{
        toast.error('!لطفا سکه را مجدداً اسکن کنید');
      }
    }
  return (
    <CacheProvider value={cacheRtl}>
        <div className='backgroundTemplate'>
          <Box sx={style.headerContainer}>
            <Stack  spacing={2}>
              <Alert severity="info" sx={{fontWeight:'bold',fontSize:'1.5rem'}}>نمایش اطلاعات مشتری</Alert>
            </Stack>
          </Box>
          {location.state?
            <Box sx={style.txtField}>
              <Box sx={isMobile?style.mobileGridBox :isTablet?style.tabletGridBox :style.gridBox}>
                  <Box sx={style.field}>
                      <span>نام:</span>
                      <span style={style.stateHandler}>{location.state.firstName}</span>
                  </Box>
                  <Box sx={style.field}>
                      <span>نام خانوادگی:</span>
                      <span style={style.stateHandler}>{location.state.lastName}</span>
                  </Box>
                  <Box sx={style.field}>
                      <span>کد ملی:</span>
                      <span style={style.stateHandler}>{location.state.nationalCode}</span>
                  </Box>
                  <Box sx={style.field}>
                      <span>شماره تماس:</span>
                      <span style={style.stateHandler}>{location.state.phoneNumber}</span>
                  </Box>
                  <Box sx={style.field}>
                      <span>آدرس:</span>
                      <span style={style.addressHandler} onClick={()=>setOpenModal(true)}>
                        {
                        location.state.address?.length>50?
                          location.state.address.substr(0,50)+' ...':
                          location.state.address
                        }
                      </span>
                  </Box>
                  <Box>
                    <Box sx={style.field}>
                    <span style={{paddingLeft:'3px'}}>نوع سکه:</span>
                  <FormControl >
                    {/* <InputLabel id="demo-simple-select-label" 
                    sx={{
                      color:'#fff',
                      '.muirtl-ry6wpw-MuiFormLabel-root-MuiInputLabel-root .Mui-focused':{
                        color:'#fff !important'
                      }
                    }}>نوع سکه</InputLabel> */}
                    <Select
                      labelId="demo-simple-select-label"
                      id="demo-simple-select"
                      // label='نوع سکه'
                      value={coinType}
                      onChange={(e)=>setcoinType(e.target.value)}
                      sx={{
                        color: "white",
                        '.MuiOutlinedInput-notchedOutline': {
                          borderColor: '#fff !important',
                        },
                        '.muirtl-ry6wpw-MuiFormLabel-root-MuiInputLabel-root.Mui-focused ':{
                          color:'#fff !important',
                        },
                        
                        '&.Mui-focused .MuiOutlinedInput-notchedOutline': {
                          borderColor: '#fff !important',
                        },
                        '&:hover .MuiOutlinedInput-notchedOutline': {
                          borderColor: '#fff !important' ,
                        },
                        '.MuiSvgIcon-root ': {
                          fill: "white !important",
                        }
                      }}
                      size='small'
                    >
                      <MenuItem value={'0'}>سکه طرح جدید</MenuItem>
                      <MenuItem value={'1'}>سکه طرح قدیم</MenuItem>
                      <MenuItem value={'2'}>سکه تمام بهار آزادی</MenuItem>
                      <MenuItem value={'3'}>نیم سکه بهار آزادی</MenuItem>
                      <MenuItem value={'4'}>ربع سکه بهار آزادی</MenuItem>
                    </Select>
                  </FormControl>
                    </Box>
                  </Box>
                  <Box sx={style.field}>
                    {location.state.assigned || isAssigned?
                    <Typography variant="h6"  sx={{color:'#fff'}}>سکه ثبت شده است</Typography>:
                    <Button variant="contained" color="secondary" onClick={handleSubmit} sx={{backgroundColor:'#cd6019'}}>
                      ثبت سکه
                    </Button>
                    }
                  </Box>
              </Box>
            </Box>
            :
            <Box sx={style.headerContainer}>
              <Stack  spacing={2}>
                <Alert severity="warning" sx={{fontWeight:'bold',fontSize:'1.5rem'}}>داده ای یافت نشد. لطفا مجدداً سکه را اسکن کنید!</Alert>
              </Stack>
            </Box>
          }
        </div>
        <Modal
          open={openModal}
          onClose={()=>setOpenModal(false)}
        >
          <Box sx={isDesktop?style.modalStyle:style.modalMobileStyle}>
            <Typography variant="h6" color="initial" sx={{fontWeight:'bold'}}>آدرس </Typography>
            <Typography variant='body1' color="initial" sx={{border:'2px solid #bdbdbd',borderRadius:'5px',wordBreak:'break-word',padding:'1rem'}}>{location.state?.address}</Typography>
          </Box>
        </Modal>
    </CacheProvider>
  )
}

export default ShowCustomerInfo
