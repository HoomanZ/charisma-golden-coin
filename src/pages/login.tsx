import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import '../css/login.css';
import "../css/main.css";
import postReq from '../interface/postRequestinterface'
import {  postRequest } from '../request';


interface userLogin{
  username: string;
  password: string;
}

function Login() {
  const navigate = useNavigate();
  const [userLogin, setUserLogin] = useState<userLogin>({
    username: '',
    password: '',
  });
  const handleChangeName =(e)=>{
    setUserLogin({
      ...userLogin,
      [e.target.name]: e.target.value,
    });
  }
  
  const handleSubmit = async(e)=>{
    e.preventDefault();
    const {data,error,lastUrl} :postReq = await postRequest(`${process.env.REACT_APP_BASE_URL}/User/login`,userLogin) as postReq;
    if(!error){
      toast.success("!ورود با موفقیت انجام شد");
      localStorage.setItem("goldenCoinToken", 'hello');
      if(lastUrl){
        navigate(lastUrl.split(`${process.env.REACT_APP_BASE_WEBSITE_ADDRESS}`).at(-1)!);
      }else{
        navigate('/welcomeUser');
      }
    }else{
      toast.error(error);
    }
  }
  
  return (
    <div>
        <div className="background">
            <div className="shape"></div>
            <div className="shape"></div>
        </div>
        <form className='loginForm'>
            <h3 className='loginTitle'>سامانه خرید آنلاین سکه صرافی کاریزما</h3>

            <label className='loginLable' htmlFor="username">نام کاربری</label>
            <input className='loginInput' name='username' value={userLogin.username} onChange={e=>handleChangeName(e)} type="text" placeholder="نام کاربری" id="username"/>

            <label className='loginLable' htmlFor="password">رمز عبور</label>
            <input className='loginInput' name='password' value={userLogin.password} onChange={e=>handleChangeName(e)} type="password" placeholder="رمز عبور" id="password"/>

            <button className='loginbtn' onClick={handleSubmit}>ورود</button>
            
        </form>
        
    </div>
  )
}

export default Login