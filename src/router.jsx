import React from 'react';
import { Routes, Route } from "react-router-dom";
import GetCoin from './pages/getCoin.tsx';
import Login from './pages/login.tsx';
import ShowCustomerInfo from './pages/showCustomerInfo.tsx';
import UserSendInfo from './pages/userSendInfo.tsx';
import Welcome from './pages/welcome.tsx';

function Router() {
  return (
    <div>
        <Routes>
          <Route path="/GetCoin/:coinId" element={<GetCoin />} />
          <Route path="/userSendInfo" element={<UserSendInfo />} /> 
          <Route path="/login" element={<Login />} />
          <Route path="/customerInfo" element={<ShowCustomerInfo />} />
          <Route path="/welcomeUser" element={<Welcome />} />
          <Route path="*" element={<Login />} />
        </Routes>
    </div>
  )
}

export default Router