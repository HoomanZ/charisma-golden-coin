import React,{useEffect} from 'react';
import './css/login.css';
import 'react-toastify/dist/ReactToastify.css';
import { createTheme,ThemeProvider } from '@mui/material/styles';
import {ToastContainer} from 'react-toastify';
import Router from './router';
import './css/app.css';
import IconButton from '@mui/material/IconButton'
import PowerSettingsNewIcon from '@mui/icons-material/PowerSettingsNew';
import { Fab, Tooltip } from '@mui/material';
import { TokenGetter } from './pages/tokenGetter.tsx';
import { useLocation } from 'react-router-dom';

const theme = createTheme({
  palette: {
    primary: {
      main: '#0C3148'
    },
    secondary: {
      main: '#b50838'
    },
  },
  typography: {
    fontFamily: [
      'IranSans'
    ].join(','),
  },
  direction:'rtl'
});



function App() {
  const location = useLocation();
  const logOutHandler=()=>{
    localStorage.removeItem('goldenCoinToken');
    window.location.replace('/login');
  }
  return (
    <ThemeProvider theme={theme}>
        <div>
          <ToastContainer position="top-left" newestOnTop />
          <TokenGetter/>
          {
          location.pathname !== '/' && location.pathname !=='/login'?
          <Tooltip title="خروج">
            <Fab 
              color='secondary' 
              onClick={logOutHandler}
              sx={{position:'absolute',left:'5px',top:'5px'}}
              >
              <PowerSettingsNewIcon/>
            </Fab>
          </Tooltip>
              :null}
          <Router />
          {/* <Home/> */}
        </div>
      </ThemeProvider>
  );
}

export default App;
