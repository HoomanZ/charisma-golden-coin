import { configureStore } from '@reduxjs/toolkit';
import requestCode from './redux/coinId';
import saveURL from './redux/saveURL';

export const store = configureStore({
  reducer: {
    requestCode:requestCode,
  },
})
export type RootState = ReturnType<typeof store.getState>