import axios from'axios';
import { toast } from 'react-toastify';
import {configureStore} from '@reduxjs/toolkit';
import {token} from './pages/tokenGetter.tsx'

const initialState = {
    url:'',
    request:''
}
function getCurrentURL(){
    return{
        type:'ADD_URL',
        url:window.location.href
    }
}
function reduxRequest(req){
    return{
        type:'REQUEST',
        request:req
    }
}
function reducer (state= initialState,action){
    switch (action.type) {
        case 'ADD_URL':
            return{
                ...state,
                url:action.url
            }
        case 'REQUEST':
            return{
                ...state,
                request:action.request
            }
            default:
                return state
            }
}
        
const store = configureStore({reducer});
export const getRequest=async(reqURL)=>{
        try{
            console.log(token)
            const { status, data } = await axios.get(
                `${reqURL}`,
                token ? { headers: { Authorization: `Bearer ${token}` } } : null
            );
            if(status === 200){
                return({data:data});
            }
        }catch(err){
            if(err.response.status === 401){
                if(store.getState().request === reqURL){
                    return
                }else{
                    store.dispatch(getCurrentURL());
                    store.dispatch(reduxRequest(reqURL));
                    return({error:'!لطفا دوباره وارد شوید'});
                }
            }
            else if(err.response.status === 404){
                return({error:'!درخواست مورد نظر یافت نشد'});
            }
            else if(err.response.status === 500){
                return({error:'!خطا در سرور رخ داد'});
            }
            else if(err.response.status === 400){
                return({error:'!درخواست مورد نظر امکان پذیر نیست'});
            }
        }    
} 

export const postRequest = async(reqURL,postData)=>{
    try{
        const { status, data } = await axios.post(
            `${reqURL}`,postData,
            token ? { headers: { Authorization: `Bearer ${token}` } } : null
        );
        if(status === 200){
            if(store.getState().url){
                return({
                    data:data,
                    message:'ok',
                    lastUrl:store.getState().url
                })
            }else{
                return({data:data,message:'ok'});
            }
        }
    }catch(err){
        if(err.response.status === 401){
            if(document.URL.split('/').at(-1) === 'login'){
                toast.error('!نام کاربری یا رمز عبور اشتباه است')
            }else{
                setTimeout(() => {
                    window.location.replace('/login');
                }, 2000);
                toast.error('!لطفا دوباره وارد شوید');
            }
        }
        else if(err.response.status === 404){
            return({error:'!درخواست مورد نظر یافت نشد'});
        }
    }
}

export const deleteRequest=async(reqURL)=>{
    try{
        const data= await axios.delete(
            `${reqURL}`,
            token ? { headers: { Authorization: `Bearer ${token}` } } : null
        );
        if(data.status === 200){
            return({status:data.status,error:''})
        }
    }catch(err){
        if(err.response.status === 401){
            setTimeout(() => {
                window.location.replace('/login');
            }, 2000);
            toast.error('!لطفا دوباره وارد شوید')
        }
        else if(err.response.status === 404){
            return({error:'!درخواست مورد نظر یافت نشد'});
        }
        else if(err.response.status === 500){
            return({error:'!خطا در سرور رخ داد'});
        }
        else if(err.response.status === 400){
            return({error:'!درخواست مورد نظر مکان پذیر نیست'});
        }
    }
} 
